module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `
        @import "~@/scss/base/_fonts.scss";
        `
      }
    }
  }
}
