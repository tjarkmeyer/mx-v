export default {

  data () {
    return {
      tables: [], // contains rows from the spreadsheet
      userName: '',
      nameOfTable: '',
      validRowForSelectedOption: [], // based on selected option
      key: '', // key of the spreadsheet
      changedKey: '', // variable to switch keys
      page: '1', // page of the spreadsheet
      isValidKey: false, // enables button to connect if true
      error: '', // error that is displayed
      errorList: [ // list of errors
        { invalidKey: 'Bitte gib einen gültigen Key für das freigegebene Dokument ein.', usedMethod: 'getDataFromSheet' },
        { emptyColumn: 'Es muss jede Spalte ausgefüllt werden.', usedMethod: 'createProperArrayForSelection' },
        { useChromeInstead: 'Hinweis: Für eine optimale Printansicht empfiehlt es sich Google Chrome zu benutzen.', usedMethod: 'checkIfGoogleChrome' },
        { chargeTooLong: 'Charge und Materialnummer können nicht zusammen länger als 23 Ziffern sein.', usedMethod: 'calculateBarcode' }
      ],
      isConnected: false, // used for the connectionState
      columnsOfTable: [ // order of the spreedsheets (hard coded aka lazy dude)
        'Bezeichner',
        'Firma',
        'Versandtyp',
        'Fortlaufende Kartonnummer',
        'Artikelbezeichnung',
        'Anzahl pro Paket',
        'Charge',
        'Datum',
        'Materialnummer',
        'Status',
        'Startwert des Etiketts',
        'Anzahl der Etiketten',
        'Endwert des Etiketts'
      ]
    }
  }
}
