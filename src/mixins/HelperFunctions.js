
export default {

  methods: {

    // Reset all values
    resetValues () {
      this.tables = []
      this.validRowForSelectedOption = []
      this.isValidKey = false
      this.key = ''
      this.changedKey = ''
      this.error = ''
      this.isConnected = false
      this.userName = ''
      this.nameOfTable = ''
      this.changeStateOfInput('#switchKey', false)
      this.$store.dispatch('setSelectedOption', 0)
    },

    // Reset values for update data
    resetValuesForReloadData () {
      this.$store.dispatch('setSelectedOption', 0)
      this.error = ''
      this.setSelectedOptionSelect('#selectField', 0)
    },

    // To enable and disable an input
    changeStateOfInput (id, state) {
      const getInputFromSwitchKey = document.querySelector(id)
      getInputFromSwitchKey.removeAttribute('disabled')
      if (state) {
        getInputFromSwitchKey.setAttribute('disabled', 'disabled')
      }
    },

    // To set selected value to option 0
    setSelectedOptionSelect (id, index) {
      const getOptionOfSelectField = document.querySelectorAll(`${id} option`)
      getOptionOfSelectField.forEach((option) => {
        option.removeAttribute('selected')
      })
      getOptionOfSelectField[index].setAttribute('selected', 'selected')
    },

    // Collect errors to tell which rows are empty
    collectErrors (selectedOption) {
      this.error = ''
      const collectErrorsArray = []
      // Collect errors
      for (let index = 0; index < this.tables.length; index++) {
        if (!this.tables[index][selectedOption]) {
          this.validRowForSelectedOption = []
          collectErrorsArray.push(this.columnsOfTable[index])
        }
        // Push value of selected row into array
        if (!collectErrorsArray.length) {
          this.validRowForSelectedOption.push(this.tables[index][selectedOption])
        }
      }
      return collectErrorsArray
    },

    // Tells which rows are missing to be filled
    createErrorString (arrayOfEmptyRows) {
      let createErrorString = ''
      for (let index = 0; index < arrayOfEmptyRows.length; index++) {
        createErrorString += (index !== arrayOfEmptyRows.length - 1) ? `${arrayOfEmptyRows[index]}, ` : `${arrayOfEmptyRows[index]}`
      }
      this.error = `${this.errorList[1].emptyColumn} (${createErrorString})`
    },

    // Parse through data to get max. number of columns
    getMaxNumberOfColumns (spreadsheetObjects) {
      for (const object of spreadsheetObjects) {
        var maxNumberOfColumns = Math.max(maxNumberOfColumns, object.gs$cell.col) || 1
      }
      return maxNumberOfColumns
    },

    // Creates a boolen of values to check if it is Chrome
    checkIfChromeBrowser (arrayToCheck) {
      const isChromium = arrayToCheck[0]
      const vendorName = arrayToCheck[1]
      const isOpera = arrayToCheck[2]
      const isIEedge = arrayToCheck[3]
      const isIOSChrome = arrayToCheck[4]

      return (isIOSChrome ||
      (isChromium !== null &&
      typeof isChromium !== 'undefined' &&
      vendorName === 'Google Inc.' &&
      isOpera === false &&
      isIEedge === false))
    },

    // Focus element of DOM
    focusElement (elementToFocus) {
      const getElement = document.querySelector(elementToFocus)
      getElement.focus()
    }
  }
}
