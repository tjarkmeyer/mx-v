import axios from 'axios'

export default {

  methods: {

    // Check for valid key while typing =>  Button enabled
    checkValidKey () {
      const getInputFromSwitchKey = document.querySelector('#switchKey').value.trim()
      if (getInputFromSwitchKey) {
        axios
          .get(`https://spreadsheets.google.com/feeds/cells/${getInputFromSwitchKey}/${this.page}/public/full?alt=json`)
          .then(response => {
            if (response.status) {
              this.isValidKey = true
            }
          })
          .catch(error => {
            if (error) { this.resetValues() }
          })
      }
    },

    // There are 3 keys: key (main), changedKey (for switching), isValidKey (for btn)
    switchDocument () {
      if (this.isConnected) {
        this.resetValues()
        document.querySelector('#switchKey').value = ''
        return
      }
      this.key = this.changedKey
      this.getDataFromSheet()
    },

    // Save input of key to local storage
    checkIfReloaded () {
      localStorage.clear()
      const getInputFromSwitchKeyValue = document.querySelector('#switchKey').value
      localStorage.setItem('key', getInputFromSwitchKeyValue)
    }
  }
}
