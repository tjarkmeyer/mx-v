export default {

  methods: {

    // Start creating DOM elements
    createPDF () {
      this.resetHtmlString()
      this.createLabelsForPrint()
      window.print()
    }
  }
}
