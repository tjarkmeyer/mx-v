export default {

  methods: {

    // Iterates times the numbers of labels
    createLabelsForPrint () {
      const getStartValueOfLabel = +document.querySelector('#firstLabel').textContent
      const getNumberOfLabels = +document.querySelector('#numberOfLabels').textContent
      for (let numberForLabel = getStartValueOfLabel; numberForLabel < getNumberOfLabels + getStartValueOfLabel; numberForLabel++) {
        this.createLabelElementForDOM(numberForLabel)
      }
      this.addInfoPage()
    },

    // Creates a label container
    createLabelElementForDOM (numberForLabel) {
      const getLabelContainer = document.querySelector('.htmlContent')
      this.htmlString = `<div class="htmlContentWrapper"><div class="mx-container-content" data-print="yes-print"><div class="mx-first-row"><span class="mx-first-row__package-number">160</span><span class="mx-first-row__type">Ware</span><span class="mx-first-row__start-value">${numberForLabel}</span></div><div class="mx-second-row"><div class="mx-second-row-first-line"><span>sleep.ink Schlafdrink mit Melatonin, 1x40ml</span><span>240</span></div><div class="mx-second-row-second-line"><span class="mx-second-row-second-line__title">Charge:</span><span>123456</span></div><div class="mx-second-row-third-line"><span class="mx-second-row-third-line__title">Verw. bis:</span><span>12/2022</span><div class="mx-second-row-third-line-charge"><span class="mx-second-row-third-line__charge">15265230</span></div></div></div><div class="mx-third-row"><svg id="barcode" jsbarcode-fontoptions="bold" width="464px" height="142px" x="0px" y="0px" viewBox="0 0 464 142" xmlns="http://www.w3.org/2000/svg" version="1.1" style="transform: translate(0,0)"><rect x="0" y="0" width="464" height="142" style="fill:#ffffff;"></rect><g transform="translate(10, 10)" style="fill:#000000;"><rect x="0" y="0" width="4" height="100"></rect><rect x="6" y="0" width="2" height="100"></rect><rect x="12" y="0" width="6" height="100"></rect><rect x="22" y="0" width="2" height="100"></rect><rect x="26" y="0" width="6" height="100"></rect><rect x="36" y="0" width="4" height="100"></rect><rect x="44" y="0" width="6" height="100"></rect><rect x="54" y="0" width="2" height="100"></rect><rect x="60" y="0" width="4" height="100"></rect><rect x="66" y="0" width="4" height="100"></rect><rect x="72" y="0" width="6" height="100"></rect><rect x="84" y="0" width="2" height="100"></rect><rect x="88" y="0" width="4" height="100"></rect><rect x="94" y="0" width="4" height="100"></rect><rect x="100" y="0" width="4" height="100"></rect><rect x="110" y="0" width="2" height="100"></rect><rect x="114" y="0" width="4" height="100"></rect><rect x="122" y="0" width="6" height="100"></rect><rect x="132" y="0" width="2" height="100"></rect><rect x="140" y="0" width="2" height="100"></rect><rect x="144" y="0" width="4" height="100"></rect><rect x="154" y="0" width="6" height="100"></rect><rect x="166" y="0" width="2" height="100"></rect><rect x="170" y="0" width="4" height="100"></rect><rect x="176" y="0" width="6" height="100"></rect><rect x="184" y="0" width="2" height="100"></rect><rect x="188" y="0" width="8" height="100"></rect><rect x="198" y="0" width="4" height="100"></rect><rect x="204" y="0" width="4" height="100"></rect><rect x="212" y="0" width="4" height="100"></rect><rect x="220" y="0" width="4" height="100"></rect><rect x="226" y="0" width="4" height="100"></rect><rect x="234" y="0" width="4" height="100"></rect><rect x="242" y="0" width="4" height="100"></rect><rect x="248" y="0" width="4" height="100"></rect><rect x="256" y="0" width="4" height="100"></rect><rect x="264" y="0" width="4" height="100"></rect><rect x="270" y="0" width="4" height="100"></rect><rect x="278" y="0" width="4" height="100"></rect><rect x="286" y="0" width="4" height="100"></rect><rect x="292" y="0" width="4" height="100"></rect><rect x="300" y="0" width="4" height="100"></rect><rect x="308" y="0" width="4" height="100"></rect><rect x="314" y="0" width="4" height="100"></rect><rect x="322" y="0" width="4" height="100"></rect><rect x="330" y="0" width="4" height="100"></rect><rect x="336" y="0" width="4" height="100"></rect><rect x="344" y="0" width="4" height="100"></rect><rect x="352" y="0" width="4" height="100"></rect><rect x="358" y="0" width="4" height="100"></rect><rect x="366" y="0" width="4" height="100"></rect><rect x="374" y="0" width="4" height="100"></rect><rect x="380" y="0" width="4" height="100"></rect><rect x="388" y="0" width="4" height="100"></rect><rect x="396" y="0" width="6" height="100"></rect><rect x="404" y="0" width="4" height="100"></rect><rect x="414" y="0" width="2" height="100"></rect><rect x="418" y="0" width="4" height="100"></rect><rect x="428" y="0" width="6" height="100"></rect><rect x="436" y="0" width="2" height="100"></rect><rect x="440" y="0" width="4" height="100"></rect><text style="font: 20px monospace" text-anchor="middle" x="222" y="122">15265230123456</text></g></svg></div></div></div>`
      getLabelContainer.innerHTML += this.htmlString
    },

    // Empty string first to have no doubles
    resetHtmlString () {
      if (this.htmlString.length) {
        document.querySelector('.htmlContent').innerHTML = ''
      }
      this.htmlString = ''
    },

    addInfoPage () {
      const getUserName = document.querySelector('.mx-header-utilities_user-name').textContent
      const getTableName = document.querySelector('.mx-header-utilities_table-name').textContent
      const getCompanyName = document.querySelectorAll('.mx-general-information strong')[0].textContent
      const getTotalNumbersOfLabels = document.querySelectorAll('.mx-general-information strong')[1].textContent
      const getFirstNumbersOfLabel = document.querySelectorAll('.mx-general-information strong')[2].textContent
      const getLastNumbersOfLabel = document.querySelectorAll('.mx-general-information strong')[3].textContent
      const getCurrentDate = this.getCurrentDate()
      const getLabelContainer = document.querySelector('.htmlContent')
      this.htmlString = `<div class="htmlContentWrapper--info-page"><div data-print="yes-print"><h1 style="margin-top: 10rem; margin-bottom: 5rem;">Allgemeine Information</h1><span style="margin-top: 5rem;">Benutzer: <strong style="text-transform: capitalize">${getUserName}</strong></span><span>Tabellenname: <strong>${getTableName}</strong></span><span>Firma: <strong>${getCompanyName}</strong></span><span>Anzahl der Etiketten: <strong>${getTotalNumbersOfLabels}</strong></span><span>Startwert des Etiketts: <strong>${getFirstNumbersOfLabel}</strong></span><span>Endwert des Etiketts: <strong>${getLastNumbersOfLabel}</strong></span><span>Datum: <strong>${getCurrentDate}</strong></span></div></div>`
      getLabelContainer.innerHTML += this.htmlString
    },

    getCurrentDate () {
      let today = new Date()
      const dd = String(today.getDate()).padStart(2, '0')
      const mm = String(today.getMonth() + 1).padStart(2, '0')
      const yyyy = today.getFullYear()
      today = dd + '/' + mm + '/' + yyyy
      return today
    }
  }
}
