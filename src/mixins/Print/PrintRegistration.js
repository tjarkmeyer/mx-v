import PrintLabel from '@/components/Label/PrintLabel'
import PrintButton from '@/components/Elements/PrintButton'
import { mapGetters } from 'vuex'

export default {

  components: {
    PrintLabel,
    PrintButton
  },

  computed: {
    ...mapGetters([
      'getSelectedOption'
    ])
  }

}
