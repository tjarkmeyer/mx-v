import axios from 'axios'
import { mapGetters } from 'vuex'

export default {

  computed: {
    ...mapGetters([
      'getSelectedOption'
    ])
  },

  watch: {
    '$store.state.selectedOption' (value, oldValue) {
      this.createProperArrayForSelection(value)
    }
  },

  created () {
    this.checkIfGoogleChrome()
    this.addReloadListener()
  },

  mounted () {
    this.getLastInputOfLocalStorage()
  },

  methods: {

    // Google Chrome has a proper print view
    checkIfGoogleChrome () {
      const isChromium = window.chrome
      const winNav = window.navigator
      const vendorName = winNav.vendor
      const isOpera = typeof window.opr !== 'undefined'
      const isIEedge = winNav.userAgent.indexOf('Edge') > -1
      const isIOSChrome = winNav.userAgent.match('CriOS')
      const valuesToCheck = [isChromium, vendorName, isOpera, isIEedge, isIOSChrome]
      if (this.checkIfChromeBrowser(valuesToCheck)) {
        this.isChromeBrowser = true
        return
      }
      this.isChromeBrowser = false
      this.error = this.errorList[2].useChromeInstead
    },

    // Adds reload listener
    addReloadListener () {
      window.addEventListener('beforeunload', this.checkIfReloaded)
    },

    // Gets the last value that was typed in
    getLastInputOfLocalStorage () {
      const getInputFromSwitchKeyValue = document.querySelector('#switchKey')
      getInputFromSwitchKeyValue.value = localStorage.getItem('key')
      this.changedKey = getInputFromSwitchKeyValue.value
      this.focusElement('#switchKey')
      this.focusElement('.mx-btn')
    },

    // Get all data from spreadsheet
    getDataFromSheet () {
      axios
        .get(`https://spreadsheets.google.com/feeds/cells/${this.key}/${this.page}/public/full?alt=json`)
        .then(response => {
          const data = response.data.feed.entry
          this.userName = response.data.feed.author[0].name.$t
          this.nameOfTable = response.data.feed.title.$t
          let columnToFilter = 1
          for (let index = 0; index < this.getMaxNumberOfColumns(data); index++) {
            this.tables.push([])
            for (const object of data) {
              if (columnToFilter === +object.gs$cell.col) {
                this.tables[columnToFilter - 1].push(object.gs$cell.$t)
              }
            }
            columnToFilter++
          }
          this.changeStateOfInput('#switchKey', true)
          this.isConnected = true
          this.isValidKey = false
          this.error = ''
          return this.tables
        })
        .catch(error => {
          if (error) {
            this.isValidKey = false
            this.error = this.errorList[0].invalidKey
          }
        })
        .finally(() => console.log('%c Request to spreadsheet completed.', 'color: green; font-weight: bold;'))
    },

    // Selects values of rows that is selected from selectedOption
    createProperArrayForSelection (selectedOption) {
      if (selectedOption !== 0) {
        this.validRowForSelectedOption = []
        const arrayOfEmptyRows = this.collectErrors(selectedOption)
        if (arrayOfEmptyRows.length) {
          this.createErrorString(arrayOfEmptyRows)
        }
      }
    }
  }
}
