import TheHeader from '@/components/Layout/TheHeader'
import TheFooter from '@/components/Layout/TheFooter'
import Settings from '@/components/Settings'
import MainCanvas from '@/components/Label/MainCanvas'
import ConnectionField from '@/components/Elements/ConnectionField'
import ConnectionButton from '@/components/Elements/ConnectionButton'
import UpdateButton from '@/components/Elements/UpdateButton'
import ErrorField from '@/components/ErrorField'

export default {

  components: {
    TheHeader,
    TheFooter,
    Settings,
    MainCanvas,
    ConnectionField,
    ConnectionButton,
    ErrorField,
    UpdateButton
  }

}
