import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    selectedOption: 0
  },

  getters: {
    getSelectedOption: state => {
      return state.selectedOption
    }
  },

  mutations: {
    mutateSelectedOption (state, payload) {
      state.selectedOption = payload
    }
  },

  actions: {
    setSelectedOption: ({ commit }, payload) => {
      commit('mutateSelectedOption', payload)
    }
  },

  modules: {
  }

})
